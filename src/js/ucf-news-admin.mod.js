jQuery(function (e) {
    e(".section-input").suggest(ajaxurl + "?action=ucf-news-sections", { delay: 500, minchars: 2, multiple: !0 }), 
    e(".topic-input").suggest(ajaxurl + "?action=ucf-news-topics", { delay: 500, minchars: 2, multiple: !0 });
});
var ucf_news_upload = function (e) {
    e(".ucf_news_fallback_image_upload").click(function (a) {
        a.preventDefault();
        var t = wp
            .media({ title: "News Fallback Image", button: { text: "Upload Image" }, multiple: !1 })
            .on("select", function () {
                var a = t.state().get("selection").first().toJSON();
                e(".ucf_news_fallback_image_preview").attr("src", a.url), 
                e(".ucf_news_fallback_image_preview").show(),
                e(".ucf_news_clear_image_upload").show(),
                e(".ucf_news_fallback_message").text(''),
                e(".ucf_news_fallback_image").val(a.id);
            })
            .open();
    });

    e(".ucf_news_clear_image_upload ").click(function (a) {
        a.preventDefault();
          
        e(".ucf_news_fallback_image_preview").hide(), 
        e(".ucf_news_fallback_image_preview").attr("src", ''), 
        e(".ucf_news_fallback_message").text('No fallback image has been set'),
        e(".ucf_news_fallback_image").val('');          
          
    });
};
jQuery(document).ready(function (e) {
    ucf_news_upload(e);
});
